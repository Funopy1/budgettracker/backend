const mongoose = require('mongoose')


const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required: [true, 'First Name is Required']
    },
    lastName:{
        type:String,
        required: [true, 'Last Name is Required']
    },
    email:{
        type:String,
        required: [true, 'Email is required.']
    },
    password:{
        type:String
    },
    mobileNo: {
        type: String,
    },
    loginType: {
        type: String,
        required: [true, 'Login type is required']
    },
    categories: [{          
        categoryName:{
            type:String
        },
        categoryType:{
            type:String
        }
    }],
    record:[{
        categoryType:{
            type:String,
        }, 
        categoryName:{
            type:String
        },
        amount:{
            type:Number 
        },
        description:{
            type:String
        },
        createdOn:{
            type: Date,
            default: new Date()
        }
    }]
})

module.exports = mongoose.model('user', userSchema)



