const express = require('express');
const mongoose  = require('mongoose');
const app = express();
const cors = require('cors')

app.use(cors())

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:Hachi2829@wdc028-course-booking.naodr.mongodb.net/budget123?retryWrites=true&w=majority',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})


//Middleware
app.use(express.json());  
app.use(express.urlencoded({extended: true}))


const userRoutes = require('./routes/user')


app.use('/api/users', userRoutes)




const port = 4000 
app.listen(port,() => {console.log (`Listening on port ${port}`)})