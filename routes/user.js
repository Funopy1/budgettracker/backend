const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')

// Register 
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

//Email Exist
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

// Login
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

// Details
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})


// Google Login
router.post('/verify-google-id-token', async (req, res) => {            // async and await always partner  
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))                // magwawait muna bago magsend 
})

// Category 
router.post('/categories', (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        categoryName: req.body.categoryName,
        categoryType: req.body.categoryType
    };
    UserController.createCategory(params).then((result) => res.send(result));
});

module.exports = router; 



//create records
router.post('/add-record', auth.verify, (req,res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        categoryName: req.body.categoryName,
        categoryType: req.body.categoryType,
        amount: req.body.amount,
        description: req.body.description
    }
    UserController.addRecord(params).then(result => res.send(result)) 
})

